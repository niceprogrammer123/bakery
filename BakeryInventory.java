public class BakeryInventory extends Application {
 
     TextField field0 = new TextField();
     TextField field1 = new TextField();
     TextField field2 = new TextField();
     TextField field3 = new TextField();
     TextField field4 = new TextField();
     TextField field5 = new TextField();
     HBox hBox = new HBox(80);
     BorderPane insideAdd = new BorderPane();
     GridPane gPane = new GridPane();
                                      
     File file = new File("C:/input.txt");
     PrintWriter out;      
     
     Calendar c = Calendar.getInstance();
     
     double totalPrice;
    
     
    @Override
    public void start(Stage primaryStage) {
 
        final BorderPane root = new BorderPane();
 
        MenuBar menuBar = new MenuBar();
 
        menuBar.prefWidthProperty().bind(primaryStage.widthProperty());
        root.setTop(menuBar);
 
        // File menu - new, exit
        Menu selectionItems = new Menu("Items");       
        MenuItem newItem = new MenuItem("New");   
        MenuItem exitItem = new MenuItem("Exit");
 
        exitItem.setOnAction(new EventHandler<ActionEvent>() {
        @Override public void handle(ActionEvent e) {               
            Platform.exit();
            }
        });                  
       
        newItem.setOnAction(new EventHandler<ActionEvent>() {      
        
            @Override
            public void handle(ActionEvent event) { 
                
                // This is to avoid duplicate when opening the menu
                    hBox.getChildren().clear();
                    gPane.getChildren().clear();
                    field0.clear();
                    field1.clear();
                    field2.clear();
                    field3.clear();
                    field4.clear();
                    field5.clear();
                   
                    insideAdd.setTop(gPane);
                    gPane.setAlignment(Pos.TOP_CENTER);
                    gPane.setPadding(new Insets(80, 80, 80, 80));
                    gPane.setHgap(100);
                    gPane.setVgap(20);
                    
                    Text text0 = new Text(30, 30, "Date");
                    text0.setFont(Font.font("Courier", FontWeight.BOLD, 34));
                    gPane.add(text0, 0, 0);
 
                    Text text1 = new Text(30, 30, "Item Name");
                    text1.setFont(Font.font("Courier", FontWeight.BOLD, 34));
                    gPane.add(text1, 1, 0);
 
                    Text text2 = new Text(30, 30, "   Price");
                    text2.setFont(Font.font("Courier", FontWeight.BOLD, 34));
                    gPane.add(text2, 2, 0); 
                    
                    Text text3 = new Text(30, 30, "Quantity");
                    text3.setFont(Font.font("Courier", FontWeight.BOLD, 34));
                    gPane.add(text3, 3, 0);
                    
                   
                    Text text4 = new Text(30, 30, "Receipt");
                    text4.setFont(Font.font("Courier", FontWeight.BOLD, 34));
                    gPane.add(text4, 4, 0);       
                    
                    Text text5 = new Text(30, 30, "Comment");
                    text5.setFont(Font.font("Courier", FontWeight.BOLD, 34));
                    gPane.add(text5, 5, 0);       
                    
                    gPane.add(field0, 0, 2);
                    gPane.add(field1, 1, 2);
                    gPane.add(field2, 2, 2);
                    gPane.add(field3, 3, 2);
                    gPane.add(field4, 4, 2);
                    gPane.add(field5, 5, 2);
 
                   // hBox.setPadding(new Insets(230, 100, 30, 900));
                    hBox.setAlignment(Pos.CENTER);
                    
                    Button add = new Button("Add");
                    add.setFont(Font.font("Courier", FontWeight.BOLD, 20));
 
                    Button cancel = new Button("Cancel");
                    cancel.setFont(Font.font("Courier", FontWeight.BOLD, 20));
                   
                    add.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent e) {      
                            try
                            {                
                               totalPrice = Double.parseDouble(field2.getText()) * Double.parseDouble(field3.getText());

                               out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
                               out.println(field0.getText() + "," + field1.getText() + ","
                               + field2.getText() + "," + field3.getText() +
                                       "," + totalPrice + "," + field4.getText() + "," + field5.getText());
                                
                                field0.clear();
                                field1.clear();
                                field2.clear();
                                field3.clear();
                                field4.clear();
                                field5.clear();
                                out.close();
                            }
                            catch (IOException b)
                            {
                            }
                        }
                    });
                    cancel.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent e) {      
                            hBox.getChildren().clear();
                            gPane.getChildren().clear();  
                            field0.clear();
                            field1.clear();
                            field2.clear();
                            field3.clear();
                            field4.clear();
                            field5.clear();
                        }
                    });
                    
                    Button clear = new Button("Clear");
                    clear.setFont(Font.font("Courier", FontWeight.BOLD, 20));
                   
                    clear.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent e) {                                  
                            field0.clear();
                            field1.clear();
                            field2.clear();
                            field3.clear();
                            field4.clear();                                                                                                                                            
                            field5.clear();
                        }
                    });
                                        
                    hBox.getChildren().addAll(add, clear, cancel);
                    insideAdd.setCenter(hBox);
                    root.setCenter(insideAdd);                     
                } 
        });

    selectionItems.getItems().addAll(newItem,
        new SeparatorMenuItem(), exitItem);
 
    menuBar.getMenus().addAll(selectionItems);
    Scene scene = new Scene(root, 300, 250, Color.WHITE);
    primaryStage.setScene(scene);
    primaryStage.show();
    primaryStage.setMaximized(true);   
  }  
             
  public static void main(String[] args) throws Exception {
    launch(args); 
    
  } 
}

